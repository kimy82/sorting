package co.uk.shibsted.helpers;

import java.text.Normalizer;
import java.util.Comparator;

/**
 * Alphabetical comparator.
 */
public class StringComparator implements Comparator<String> {

	private final boolean	normalise;

	public StringComparator(boolean normalise) {
		this.normalise = normalise;
	}

	@Override
	public int compare(String o1, String o2) {
		String o1N = o1;
		String o2N = o2;

		if (normalise) {
			o1N = Normalizer.normalize(o1, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
			o2N = Normalizer.normalize(o2, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		}
		
		return o1N.compareTo(o2N);
	}
}

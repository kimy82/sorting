package co.uk.shibsted.helpers;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Wrapper for {@link BufferedReader} keeping last line in cache.
 */
public class BufferReaderWrapper {

	public BufferedReader	bufferedReader;
	private String			cacheLine;

	public BufferReaderWrapper(BufferedReader bufferedReader) throws IOException {
		this.bufferedReader = bufferedReader;
		loadNextLine();
	}

	public boolean isEmpty() {
		return this.cacheLine == null;
	}

	public String getCachedLine() {
		return this.cacheLine;
	}

	public void loadNextLine() throws IOException {
		this.cacheLine = this.bufferedReader.readLine();
	}
}

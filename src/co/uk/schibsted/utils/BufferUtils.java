package co.uk.schibsted.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

import co.uk.schibsted.exceptions.FileSorterException;

public class BufferUtils {
	
	/**
	 * @param file from where we generated the writer.
	 * @return {@link BufferedWriter} of the input file.
	 * @throws FileSorterException
	 */
	public static BufferedWriter getWriter(File file) throws FileSorterException {
		try {
			return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, false), Charset.defaultCharset()));
		} catch (FileNotFoundException e) {
			throw new FileSorterException("File not found while getting Writer.", e);
		}
	}
	
	/**
	 * 
	 * @param file  from where we generated the reader.
	 * @return {@link BufferedReader} of the input file.
	 * @throws FileSorterException
	 */
	public static BufferedReader getReader(File file) throws FileSorterException {
		try {
			return new BufferedReader(new InputStreamReader(new FileInputStream(file), Charset.defaultCharset()));
		} catch (FileNotFoundException e) {
			throw new FileSorterException("File not found while getting Reader.", e);
		}
	}
	/**
	 * 
	 * @param tmpFileBw {@link BufferedWriter}
	 */
	public static <T extends Closeable> void closeBuffer(T buffer) {
		try {
			buffer.close();
		} catch (IOException e) {
			throw new FileSorterException("Could not close the writer", e);
		}
	}
}

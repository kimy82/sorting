package co.uk.schibsted.utils;

import co.uk.shibsted.helpers.Os;

public class MemoryUtils {

	/**
	 * First clean allocated memory and then gets the free memory available that we can use in the process.
	 * 
	 * @return memory available in bytes.
	 */
	public static long getAvailableMemory() {
		System.gc();
		return Runtime.getRuntime().freeMemory();
	}

	/**
	 * 
	 * @param line {@link String}
	 * @param os {@link Os}
	 * @return the size of the string in bytes
	 */
	public static long getSizeOf(String line, Os os) {
		return (line.length() * 2) + MemoryUtils.getOverhead(os);
	}
	

	private static long getOverhead(Os os) {
		if (os == Os.x32) {
			return 36;
		} else {
			return 60;
		}
	}

}

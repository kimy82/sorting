package co.uk.schibsted.utils;

import java.io.File;
import java.io.IOException;

import co.uk.schibsted.exceptions.FileSorterException;


public class FileUtils {

	/**
	 * Gets the best file partition size. The size of a char in java is 2 bytes. That is why we at least need twice the size of the partitioned file.
	 * 
	 * @param sizeoffile
	 * @return affordable block size in bytes.
	 */
	public static long getSizeOfFilePartition(final long sizeoffile) {
		final long memoryAvailable = MemoryUtils.getAvailableMemory();
		System.out.println("MEMORY AVAILABLE FOR THE PROCESS: " + memoryAvailable);
		return memoryAvailable / 2;
	}
	
	/**
	 * Creates a temporary file that gets delete when process is done.
	 * 
	 * @return
	 */
	public static File createTmpFile() {
		try {
			File newtmpfile = File.createTempFile("partition", ".tmp");
			newtmpfile.deleteOnExit();
			return newtmpfile;
		} catch (IOException e) {
			throw new FileSorterException("Problem creating tmp file", e);
		}
	}
}

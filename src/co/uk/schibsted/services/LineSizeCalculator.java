package co.uk.schibsted.services;

/**
 * A string is composed of chars which need 2 bytes of allocated memory.
 */
public class LineSizeCalculator {

	/**
	 * 
	 * @param line {@link String}
	 * @param os {@link Os}
	 * @return the size of the string in bytes
	 */
	public static long getSizeOf(String line, Os os) {
		return (line.length() * 2) + LineSizeCalculator.getOverhead(os);
	}

	private static long getOverhead(Os os) {
		if (os == Os.x32) {
			return 36;
		} else {
			return 60;
		}
	}

}

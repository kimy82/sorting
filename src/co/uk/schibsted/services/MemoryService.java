package co.uk.schibsted.services;

public class MemoryService {

	/**
	 * First clean allocated memory and then gets the free memory available that we can use in the process.
	 * 
	 * @return memory available in bytes.
	 */
	public static long getAvailableMemory() {
		System.gc();
		return Runtime.getRuntime().freeMemory();
	}
}

package co.uk.schibsted.services;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Wrapper for {@link BufferedReader} keeping last line in cache.
 */
public class CachedFirstLineFileBuffer {

	public BufferedReader	bufferedReader;
	private String			cacheLine;

	public CachedFirstLineFileBuffer(BufferedReader bufferedReader) throws IOException {
		this.bufferedReader = bufferedReader;
		getNextLine();
	}

	public boolean isEmpty() {
		return this.cacheLine == null;
	}

	public String getCachedLine() {
		return this.cacheLine;
	}

	public void getNextLine() throws IOException {
		this.cacheLine = this.bufferedReader.readLine();
	}
}

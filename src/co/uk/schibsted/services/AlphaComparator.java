package co.uk.schibsted.services;

import java.util.Comparator;

/**
 * Alphabetical comparator.
 */
public class AlphaComparator implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		return o1.compareTo(o2);
	}
}

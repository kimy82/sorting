package co.uk.schibsted.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

import co.uk.schibsted.exceptions.FileSorterException;
import co.uk.schibsted.utils.BufferUtils;
import co.uk.shibsted.helpers.StringComparator;
import co.uk.shibsted.helpers.BufferReaderWrapper;

/**
 * 
 * Main class that merges the generated sorted files. Creates the output file named sorted.txt in the same folder as the program is executed.
 *
 */
public class FileMerger {

	private final List<File>			files;
	private ArrayList<BufferedReader>	bufferReaders	= new ArrayList<BufferedReader>();
	private static final String			OUTPUT			= "sorted.txt";
	private final boolean				normalise;

	public FileMerger(List<File> files, boolean normalise) {
		this.files = files;
		this.bufferReaders.clear();
		this.normalise = normalise;
	}

	public void mergePartitonFiles() throws FileSorterException {
		try {
			this.files.forEach(file -> bufferReaders.add(BufferUtils.getReader(file)));

			File outputFile = new File(OUTPUT);

			if (!outputFile.exists())
				outputFile.createNewFile();

			BufferedWriter outputFileWriter = BufferUtils.getWriter(outputFile);

			PriorityQueue<BufferReaderWrapper> buffersPriorityQueue = createPriorityQueue();

			bufferReaders.forEach(buffer -> addBuffersToQueue(buffer, buffersPriorityQueue));

			merge(outputFileWriter, buffersPriorityQueue);

			outputFileWriter.close();
		} catch (IOException e) {
			throw new FileSorterException("Problem Merging files", e);
		}
	}

	/**
	 * Merge files.
	 * 
	 * @param outputFileWriter
	 * @param buffersPriorityQueue
	 */
	private void merge(BufferedWriter outputFileWriter, PriorityQueue<BufferReaderWrapper> buffersPriorityQueue) throws FileSorterException {
		try {
			while (buffersPriorityQueue.size() > 0) {
				BufferReaderWrapper headBuffer = buffersPriorityQueue.poll();
				String line = headBuffer.getCachedLine();
				outputFileWriter.write(line);
				outputFileWriter.newLine();
				headBuffer.loadNextLine();
				if (headBuffer.isEmpty()) {
					headBuffer.bufferedReader.close();
				} else {
					buffersPriorityQueue.add(headBuffer);
				}
			}
		} catch (IOException e) {
			throw new FileSorterException("Problem Merging files", e);
		}
	}

	/**
	 * Creates priority queue with alphabetical priority.
	 * 
	 * @return {@link PriorityQueue}
	 */
	private PriorityQueue<BufferReaderWrapper> createPriorityQueue() {
		return new PriorityQueue<BufferReaderWrapper>(bufferReaders.size(), new Comparator<BufferReaderWrapper>() {
			@Override
			public int compare(BufferReaderWrapper bufferI, BufferReaderWrapper bufferJ) {
				return new StringComparator(FileMerger.this.normalise).compare(bufferI.getCachedLine(), bufferJ.getCachedLine());
			}
		});
	}

	/**
	 * Adds the generated buffer to the priority queue to be processed.
	 * 
	 * @param bufferToAdd {@link BufferedReader}
	 * @param buffersPriorityQueue {@link PriorityQueue}
	 * @throws FileSorterException
	 */
	private void addBuffersToQueue(final BufferedReader bufferToAdd, final PriorityQueue<BufferReaderWrapper> buffersPriorityQueue) throws FileSorterException {
		try {
			buffersPriorityQueue.add(new BufferReaderWrapper(bufferToAdd));
		} catch (IOException e) {
			throw new FileSorterException("No buffered reader can be added to priority queue", e);
		}
	}

}

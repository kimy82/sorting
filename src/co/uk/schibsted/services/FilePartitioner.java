package co.uk.schibsted.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import co.uk.schibsted.exceptions.FileSorterException;
import co.uk.schibsted.utils.BufferUtils;
import co.uk.schibsted.utils.FileUtils;
import co.uk.schibsted.utils.MemoryUtils;
import co.uk.shibsted.helpers.StringComparator;
import co.uk.shibsted.helpers.Os;

public class FilePartitioner {

	private boolean		eof	= false;
	private final File	fileToSort;
	private final long	sizeOfPatitionFile;
	private final Os	os;
	private final boolean normalise;

	public FilePartitioner(File fileToSort, Os os, boolean normalise) {
		this.fileToSort = fileToSort;
		this.os = os;
		this.sizeOfPatitionFile = FileUtils.getSizeOfFilePartition(fileToSort.length());
		this.normalise = normalise;
	}

	/**
	 * Gets partitions of {@link #fileToSort} given the available space {@link #sizeOfPatitionFile}
	 * 
	 * @return a list of temporary files.
	 * @throws FileSorterException
	 */
	public List<File> getPartitions() throws FileSorterException {

		List<File> files = new ArrayList<File>();
		BufferedReader fileToSortBf = BufferUtils.getReader(this.fileToSort);

		try {
			List<String> currentLines = new ArrayList<String>();
			while (!this.eof) {

				createSetOfLines(fileToSortBf, currentLines);
				currentLines = sortLines(currentLines);

				File newtmpfile = FileUtils.createTmpFile();
				BufferedWriter tmpFileBw = BufferUtils.getWriter(newtmpfile);

				currentLines.forEach(lined -> addLineToFile(lined, tmpFileBw));
				BufferUtils.closeBuffer(tmpFileBw);

				currentLines.clear();
				files.add(newtmpfile);
			}
		} finally {
			try {
				fileToSortBf.close();
			} catch (IOException e) {
				throw new FileSorterException("File couldn't be closed.", e);
			}
		}
		return files;
	}

	/**
	 * Creates the set of lines of the partitioned file.
	 * 
	 * @param fileToSortBf file from where we are getting the lines.
	 * @param currentLines List of lines.
	 */
	private void createSetOfLines(BufferedReader fileToSortBf, List<String> currentLines) {

		String line = null;
		long currentblocksize = 0;

		try {
			while ((currentblocksize < this.sizeOfPatitionFile) && ((line = fileToSortBf.readLine()) != null)) {
				currentblocksize += addLineToCurrentLines(currentLines, line);
			}
		} catch (IOException e) {
			throw new FileSorterException("Problem reading new line.", e);
		}

		if (line == null) {
			this.eof = true;
		}
	}

	/**
	 * Sort lines in alphabetical order.
	 * 
	 * @param currentLines
	 * @return a sorted Array
	 */
	private ArrayList<String> sortLines(List<String> currentLines) {
		return currentLines.parallelStream().sorted(new StringComparator(this.normalise)).collect(Collectors.toCollection(ArrayList<String>::new));
	}

	/**
	 * Write a new line
	 * 
	 * @param line {@link String}
	 * @param bw {@link BufferedWriter}
	 * @throws FileSorterException
	 */
	private void addLineToFile(String line, BufferedWriter bw) throws FileSorterException {
		try {
			bw.write(line);
			bw.newLine();
		} catch (IOException e) {
			throw new FileSorterException("NO line could be added to the parted tmp file.");
		}
	}

	/**
	 * Adds line to the currentLines and updates currentblocksize.
	 * 
	 * @param currentLines {@link ArrayList}
	 * @param line {@link String}
	 * @param currentblocksize {@link long}
	 * @return size of line added.
	 */
	private long addLineToCurrentLines(List<String> currentLines, String line) {
		currentLines.add(line);
		return MemoryUtils.getSizeOf(line, this.os);
	}

}

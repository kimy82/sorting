package co.uk.schibsted.services;

import java.io.File;
import java.util.List;

import co.uk.schibsted.exceptions.FileSorterException;
import co.uk.shibsted.helpers.Os;

/**
 * Main Service for sorting huge files.
 */
public class FileSorter {

	private final File	fileToSort;
	private final Os	os;
	private final boolean normalise;

	public FileSorter(File fileToSort, Os os, boolean normalise) {
		this.fileToSort = fileToSort;
		this.os = os;
		this.normalise  = normalise;
	}

	public void execcute() throws FileSorterException {
		List<File> files = new FilePartitioner(this.fileToSort, this.os, this.normalise).getPartitions();
		new FileMerger(files, this.normalise).mergePartitonFiles();
	}
}

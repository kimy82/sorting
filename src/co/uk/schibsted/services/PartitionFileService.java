package co.uk.schibsted.services;

public class PartitionFileService {

	/**
	 * Gets the best file partition size. The size of a char in java is 2 bytes. That is why we at least need twice the size of the partitioned file.
	 * 
	 * @param sizeoffile
	 * @return affordable block size in bytes.
	 */
	public static long getSizeOfFileFraction(final long sizeoffile) {
		final long memoryAvailable = MemoryService.getAvailableMemory();
		System.out.println("MEMORY AVAILABLE FOR THE PROCESS: " + memoryAvailable);
		return memoryAvailable / 2;
	}
}

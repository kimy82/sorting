package co.uk.schibsted;

import java.io.File;
import java.util.Calendar;

import co.uk.schibsted.exceptions.FileSorterException;
import co.uk.schibsted.services.FileSorter;
import co.uk.shibsted.helpers.Os;

public class Main {

	/**
	 * Main class to run.
	 * 
	 * To get a better performance you can specify the OS. ex: -os32 or -os64.
	 * 
	 * By default the os is x64 which is the safest in  order to not get an Out of memory error.
	 * 
	 * To normalise string before comparing you can specify it as an argument -normalise
	 * 
	 * Once finished it will generate within this folder an output file named sorted.txt
	 * 
	 * @param args
	 * @throws FileSorterException
	 */
	public static void main(final String[] args) throws FileSorterException {
		if (args.length == 0) {
			System.err.println("No path to file specified.");
			return;
		}

		int argIndex = 0;
		Os osChoosen = Os.x64;
		boolean normalizeComparing = false;
		
		while(args.length > argIndex + 1){
			if (args[argIndex].equals("-os32")) {
				osChoosen = Os.x32;			
			}
	
			if (args[argIndex].equals("-normalise")) {
				normalizeComparing = true;
			}
			argIndex++;
		}
		
		System.out.println("Runing against a "+ osChoosen + " and comparing " + (normalizeComparing ? "normalised string" : "without normalising string"));
		
		File fileToSort = new File(args[argIndex]);

		if (!fileToSort.exists() || fileToSort.isDirectory()) {
			System.err.println("The path set is not an existing file.");
			return;
		}

		FileSorter fileSorter = new FileSorter(fileToSort, osChoosen, normalizeComparing);

		long before = Calendar.getInstance().getTimeInMillis();

		fileSorter.execcute();

		long after = Calendar.getInstance().getTimeInMillis();

		System.out.println("Time spent sorting: " + (after - before) / 1000 + "s");

	}
}

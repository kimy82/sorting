package co.uk.schibsted.exceptions;

/**
 * 
 * Handles exceptions while sorting the file.
 *
 */
public class FileSorterException extends RuntimeException {

	private static final long	serialVersionUID	= 1L;

	public FileSorterException(String message) {
		super(message);
	}

	public FileSorterException(String message, Exception e) {
		super(message, e);
	}
}

#HUGE FILE SORTING.

##THE AIM
Alphabetical sorting of huge files that don't fit in memory.

##THE APPROACH
The approach is well documented in [wikipedia](https://en.wikipedia.org/wiki/Merge_sort)

* Divide the original file in smaller files that fit in memory.
    * The size of each file is calculated getting the free memory available at the moment of execution and divided by 2. We divide this size by 2 because a line in the file is a String in java which is composed of chars which need 2 bytes of allocated memory.
* Sort those files.
* Merge those files.

###THE FILE MERGE
* Create an alphabetical sorting priority queue to hold those files.
    * It compares the very first line.
* Get the first line from the first element in the priority queue.
* Write the line in the output file.
* Remove that line from the file.
* Set the file in the priority queue again if there are still lines to be processed.
* Follow the process till no elements in the priority queue are missing. 

##THE PERFORMANCE
* In order to get more performance there is one argument that can be specified when running the program to tell the program which OS is running on.
 
* The comparison is not against normalised strings, so that, to get a normalised comparison there is a another argument _-normalise_ that will compare using a normalised representation of the line. Setting this argument will reduce the performance of the sorting.

##RUN EXAMPLE
Within the example there is a generated jar file that can be run from command line using _java -jar sort.jar -os64 -normalise [path to file]_

##JAVA VERSION TO COMPILE
JAVA 8 